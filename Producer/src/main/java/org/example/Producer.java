package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Date;
import java.util.Properties;

public class Producer {
    public static void main(String[] args) {
        final String topic = "messages";
        final Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        try (final org.apache.kafka.clients.producer.Producer<String, String> producer = new KafkaProducer<>(props)) {
            String value = new Date().toString();
            for (int i = 0; i < 1000; i++) {
                producer.send(new ProducerRecord<>(topic, i + " message", value));
            }
            System.out.println("All messages are send");
            producer.flush();
        }
    }
}